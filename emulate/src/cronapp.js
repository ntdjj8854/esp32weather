import cron from 'node-cron'
import axios from 'axios'
import moment from 'moment'
import { API_OWM_KEY, SERVER, PORT, STATION_ID } from './config/config.js'

const lat = 39.56939
const long = 2.65024

function main(){
	console.log("-------------------")
	console.log("| Virtual ESP0001 |")
	console.log("-------------------")
	console.log("")
	console.log("Iniciando consulta ..")
	getOWData()
}

async function insertData(temp, hum, pres, station_id){
	axios.post(`${SERVER}:${PORT}/api/readings/${STATION_ID}`, {
		"temperature": temp,
		"temp_unit_id": 1,
		"humidity": hum,
		"hum_unit_id": 4,
		"pressure": pres,
		"pres_unit_id": 5
	})
	.then((response) => {
		console.log(response.data);
	}, (error) => {
		console.log(error);
	});
}

async function getOWData(){
	const url = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=${API_OWM_KEY}&units=metric`
	const response = await axios.get(url)
	console.log("Temperature: " + response.data.main.temp + ' °C')
	console.log("Humidity: " + response.data.main.humidity + ' %')
	console.log("Pressure: " + response.data.main.pressure + ' hPa')
	console.log("Fecha actual: " + moment(new Date).format("DD/MM/YY HH:mm"))
	console.log("Fecha actualización: " + moment(response.data.dt, 'X').format("DD/MM/YY HH:mm"))
	console.log(" ")
	insertData(response.data.main.temp, response.data.main.humidity, response.data.main.pressure)
}

// Cron format
// * * * * * *
// | | | | | |
// | | | | | day of week
// | | | | month
// | | | day of month
// | | hour
// | minute
// second ( optional )

cron.schedule('0,10,20,30,40,50 * * * *', function () {
	console.log("-------------------")
	console.log("| Virtual ESP0001 |")
	console.log("-------------------")
	console.log("")
	console.log("Iniciando consulta ..")
	getOWData()
})