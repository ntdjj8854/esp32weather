import { config } from "dotenv";

config()

export const PORT = process.env.PORT || 3000
export const API_OWM_KEY = process.env.API_OWM_KEY
export const SERVER = process.env.SERVER
export const STATION_ID = process.env.STATION_ID