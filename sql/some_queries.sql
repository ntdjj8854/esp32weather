select u.id , u.unit, ud.dimension from units u left join unit_dimension ud on (u.dimension_id = ud.id);

-- Obtiene todos los datos de readings.
select * from readings r ;

-- Obtiene los datos de una estacion ordenados por fecha (desc) y dimension id.
select r.datetime, r.value, ud.dimension, u.unit, u.representation 
from readings r 
inner join units u on (u.id = r.unit_id)
inner join unit_dimension ud on (ud.id = u.dimension_id)
where station_id = 'ESP0001'
order by r.datetime desc, ud.id;


-- Obtiene las ultimas lecturas de una estaci�n.
SELECT t1.dt1 as datetime, t1.temperature, t1.unit_id as temp_unit_id, t2.humidity, t2.unit_id as hum_unit_id, t3.pressure, t3.unit_id as pres_unit_id
FROM(
	(SELECT r.station_id, r.datetime as dt1, r.unit_id, r.value as temperature FROM readings r where unit_id = 1) as t1,
	(SELECT r.station_id, r.datetime as dt2, r.unit_id, r.value as humidity FROM readings r where unit_id = 4) as t2,
	(SELECT r.station_id, r.datetime as dt3, r.unit_id, r.value as pressure FROM readings r where unit_id = 5) as t3
)
WHERE t1.station_id = 'ESP0001' and dt1 = dt2 and dt2 = dt3
ORDER BY dt1 DESC LIMIT 1;

-- Obtiene las ultimas lecturas de una estación. Cada dimension es una fila.
SELECT  
	r.station_id, 
	r.datetime,
	ud.dimension,
	r.value,
	u.representation,
	u.unit,
	r.unit_id
FROM readings r 
INNER JOIN units u ON (u.id = r.unit_id) 
INNER JOIN unit_dimension ud ON (ud.id=u.dimension_id)
WHERE r.station_id = 'ESP0001'
AND r.datetime >= (
	SELECT max(datetime) 
	FROM readings r2 
	WHERE r2.unit_id = r.unit_id)
ORDER BY r.`datetime` DESC, u.id;

-- ================================================ JSON QUERIES ================================================

-- Retorna todos los readings de una estacion en formato JSON.
select json_arrayagg(json_object("station_id", t.station_id, 
   "datetime", t.datetime, "readings", t.js)) 
from (select r.station_id, r.datetime, json_arrayagg(json_object("dimension", ud.dimension, 
      "value", r.value, 
      "representation", u.representation, 
      "unit", u.unit, "unit_id", u.id)) js 
      from readings r join units u on r.unit_id = u.id 
      join unit_dimension ud on ud.id = u.dimension_id
WHERE r.station_id = 'ESP0001'
group by r.station_id, r.datetime
order by u.id ASC, r.datetime DESC) t;

-- V2
select json_arrayagg(
	json_object(
		"station_id", t.station_id, 
		"datetime", t.datetime, 
		"readings", t.js
	)
)as data
from (
	select 
		r.station_id, 
		r.datetime, 
		json_arrayagg(
			json_object(
				"dimension", ud.dimension, 
				"value", r.value, 
				"representation", u.representation, 
				"unit", u.unit, "unit_id", r.unit_id
			)
		) js 
	from readings r 
	left join units u on u.id = r.unit_id 
	left join unit_dimension ud on ud.id = u.dimension_id
	WHERE r.station_id = 'ESP0001'
	AND r.datetime BETWEEN '2022-11-25' AND '2022-11-25 23:59:59'
	group by r.station_id, r.datetime
	order by r.datetime DESC, r.unit_id ASC
) t;

-- Retorna los ultimos 20 readings de una estacion en formato JSON ordenados por datetime DESC.
select json_arrayagg(
	json_object(
		"station_id", t.station_id, 
		"datetime", t.datetime, 
		"readings", t.js
	)
)as data
from (
	select 
		r.station_id, 
		r.datetime, 
		json_arrayagg(
			json_object(
				"dimension", ud.dimension, 
				"value", r.value, 
				"representation", u.representation, 
				"unit", u.unit, "unit_id", r.unit_id
			)
		) js 
	from readings r 
	left join units u on u.id = r.unit_id 
	left join unit_dimension ud on ud.id = u.dimension_id
	WHERE r.station_id = 'ESP0001'
	group by r.station_id, r.datetime
	order by r.datetime DESC, r.unit_id ASC
	LIMIT 20
) t;

-- Retorna el último reading de la estacion.
select json_arrayagg(
	json_object(
		"station_id", t.station_id, 
		"datetime", t.datetime, 
		"readings", t.js
	)
)as "read"
from (
	select 
		r.station_id, 
		r.datetime, 
		json_arrayagg(
			json_object(
				"dimension", ud.dimension, 
				"value", r.value, 
				"representation", u.representation, 
				"unit", u.unit, "unit_id", r.unit_id
			)
		) js 
	from readings r 
	left join units u on u.id = r.unit_id 
	left join unit_dimension ud on ud.id = u.dimension_id
	WHERE r.station_id = 'ESP0001'
	group by r.station_id, r.datetime
	order by r.datetime DESC, r.unit_id ASC
	LIMIT 1
) t;

-- v2
select json_object(
		"station_id", t.station_id, 
		"datetime", t.datetime, 
		"readings", t.js
) "read"
from (
	select 
		r.station_id, 
		r.datetime, 
		json_arrayagg(
			json_object(
				"dimension", ud.dimension, 
				"value", r.value, 
				"representation", u.representation, 
				"unit", u.unit, "unit_id", r.unit_id
			)
		) js 
	from readings r 
	left join units u on u.id = r.unit_id 
	left join unit_dimension ud on ud.id = u.dimension_id
	WHERE r.station_id = 'ESP0001'
	group by r.station_id, r.datetime
	order by r.datetime DESC, r.unit_id ASC
	LIMIT 1
) t;
-- ==============================================================================================================

SELECT * FROM (
	SELECT r.datetime, r.value as 'temperature', u.representation, u.unit  
	FROM readings r
	inner join units u on (u.id = r.unit_id)
	inner join unit_dimension ud on (ud.id = u.dimension_id)
	WHERE r.station_id = 'ESP0001' 
	AND ud.dimension = 'temperature'
	AND r.datetime BETWEEN '2022-11-25' AND '2022-11-25 23:59:59'
	ORDER BY r.datetime desc
)sub order by datetime ASC;
