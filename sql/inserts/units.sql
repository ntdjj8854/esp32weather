INSERT INTO units VALUES
(1, 1,'Celsius','C'),
(2, 1,'Fahrenheit','F'),
(3, 1,'Kelvin','K'),
(4, 2,'Percentage','%'),
(5, 3,'HectoPascal','hPa'),
(6, 3,'Pascal','Pa'),
(7, 3,'Bar','bar'),
(8, 3,'Technical Atmosphere','at'),
(9, 3,'Standard Atmosphere','atm'),
(10, 3,'Pound per square inch','psi'),
(11, 4,'Lux','lx'),
(12, 5,'Watts per square metre','W/m2');
