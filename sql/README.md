# DB Structure

## Tables

### STATIONS Table

Fields:

* ID
* NAME
* DESCRIPTION
* OWNER_NAME
* OWNER_LASTNAME
* OWNER_EMAIL
* OWNER_USERNAME
* COUNTRY
* LATITUDE
* LONGITUDE
* CREATE_DATE
* MOD_DATE
* DEACTIV_DATE
* ACTIVE

|id|name|description|owner_name|owner_lastname|owner_email|owner_username|country|latitude|longitude|create_date|mod_date|deactiv_date|active|
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|ES001|Ambrosio Station|Lorem Ipsum|Ambrosio|Macaroni|ambromacaroni@example.com|amacaroni|ES|39.571625|2.650544|2022-10-09 15:00:00|2022-10-09 15:00:00|--|1|
|ES002|Station Weather|Lorem Ipsum|Natalia|Hermosilla|hermosilla@example.com|hermosilla|ES|39.7211000|2.9109300|2022-10-09 15:00:00|2022-10-16 15:00:00|2022-10-16 15:00:00|0|


Script to create the table:

```sql
DROP TABLE IF EXISTS stations;
CREATE TABLE stations (
  id VARCHAR(9) PRIMARY KEY UNIQUE NOT NULL,
  name VARCHAR(100) NOT NULL,
  description VARCHAR(255) NOT NULL,
  owner_name VARCHAR(255) NOT NULL,
  owner_lastname VARCHAR(255) NOT NULL,
  owner_email VARCHAR(200) NOT NULL,
  owner_username VARCHAR(50) NOT NULL,
  country CHAR(2) NOT NULL,
  latitude DOUBLE,
  longitude DOUBLE,
  create_date TIMESTAMP NOT NULL,
  mod_date TIMESTAMP,
  deactiv_date TIMESTAMP,
  active boolean
)ENGINE=InnoDB;
```

### SENSORS Table

Fields:

* ID
* STATION_ID
* NAME
* DESCRIPTION
* MANUFACTURER
* SENSOR_TYPE

Script to create the table:

```sql
DROP TABLE IF EXISTS sensors;
CREATE TABLE sensors (
  id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  station_id VARCHAR(9) NOT NULL,
  name VARCHAR(100) NOT NULL,
  description VARCHAR(200) NOT NULL,
  manufacturer VARCHAR(200) NOT NULL,
  sensor_type VARCHAR(100) NOT NULL,
  CONSTRAINT sensors_station_fk FOREIGN KEY (station_id) REFERENCES stations (id) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB;
```

### UNIT_DIMENSION Table

This table contains the dimension of the sensors readings.

Fields:

* ID.
* DIMENSION.

|    id    |    dimension    |
|:--------:|-----------------|
|     1    |    temperature  |
|     2    |    humidity     |
|     3    |    pressure     |
|     4    |    illuminance  |
|     5    |    irradiance   |

Script to create the table:

```sql
DROP TABLE IF EXISTS unit_dimension;
CREATE TABLE unit_dimension (
  id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  dimension VARCHAR(100) NOT NULL UNIQUE
)ENGINE=InnoDB;

INSERT INTO unit_dimension VALUES 
(1, 'temperature'),
(2, 'humidity'),
(3, 'pressure'),
(4, 'illuminance'),
(5, 'irradiance');
```

### UNITS table

This table contains the units of the different dimensions with respect to the chosen measurement system (SI, EEUU, etc).

Fields:

* ID
* DIMENSION_ID
* UNIT
* REPRESENTATION

|    id    |    dimension_id    |           unit         |    representation    |
|:--------:|:------------------:|------------------------|:--------------------:|
|     1    |          1         |         Celsius        |          °C          |
|     2    |          1         |        Fahrenheit      |          °F          |
|     3    |          1         |          Kelvin        |           K          |
|     4    |          2         |        Percentage      |           %          |
|     5    |          3         |        HectoPascal     |          hPa         |
|     6    |          3         |          Pascal        |          Pa          |
|     7    |          3         |           Bar          |          bar         |
|     8    |          3         |  Technical Atmosphere  |          at          |
|     9    |          3         |   Standard Atmosphere  |          atm         |
|    10    |          3         |  Pound per square inch |          psi         |
|    11    |          4         |           Lux          |          lx          |
|    12    |          5         | Watts per square metre |          W/m2        |

Script to create the table:

```sql
DROP TABLE IF EXISTS units;
CREATE TABLE units (
  id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  dimension_id INT NOT NULL,
  unit VARCHAR(100) NOT NULL,
  representation VARCHAR(4) NOT NULL,
  CONSTRAINT dimension_unit_fk FOREIGN KEY (dimension_id) REFERENCES unit_dimension (id) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB;

INSERT INTO units (dimension_id, unit, representation) VALUES
(1, 'Celsius', '°C'),
(1, 'Fahrenheit', '°F'),
(1, 'Kelvin', 'K'),
(2, 'Percentage', '%'),
(3, 'HectoPascal', 'hPa'),
(3, 'Pascal', 'Pa'),
(3, 'Bar', 'bar'),
(3, 'Technical Atmosphere', 'at'),
(3, 'Standard Atmosphere', 'atm'),
(3, 'Pound per square inch', 'psi'),
(4, 'Lux', 'lx'),
(5, 'Watts per square metre', 'W/m2');
```

### READINGS Table

This table stores the readings from the station.

Fields:

* ID
* STATION_ID
* UNIT_ID
* VALUE
* DATE
* TIME

|    id    |    station_id    |    unit_id    |    value    |    date    |    time    |
|:--------:|:----------------:|:-------------:|:-----------:|:----------:|:----------:|
|    1     |      ES001       |       1       |     27.5    | 2022-10-18 |  09:10:00  |
|    2     |      ES001       |       4       |     47.5    | 2022-10-18 |  09:10:00  |
|    3     |      ES001       |       5       |     1010    | 2022-10-18 |  09:10:00  |
|    4     |      ES002       |       1       |     28.5    | 2022-10-18 |  09:10:00  |
|    5     |      ES003       |       4       |      48     | 2022-10-18 |  09:10:00  |
|    6     |      ES002       |       5       |     1011    | 2022-10-18 |  09:10:00  |

Script to create the table:

```sql
DROP TABLE IF EXISTS readings;
CREATE TABLE readings (
  id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  station_id VARCHAR(9) NOT NULL,
  unit_id INT NOT NULL,
  value DOUBLE NOT NULL,
  date DATE NOT NULL,
  time TIME NOT NULL,
  CONSTRAINT readings_station_fk FOREIGN KEY (station_id) REFERENCES stations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT readings_unit_fk FOREIGN KEY (unit_id) REFERENCES units (id) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB;
```

Countries, States and Cities tables are from [Github Country State City API](https://github.com/dr5hn/countries-states-cities-database).
