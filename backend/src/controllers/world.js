import pool from "../helpers/database.js"

const getCountries = async (req, res, next) => {
	try {
		const sql = 'SELECT id, name, iso2, iso3, region, subregion, latitude, longitude, emoji, emojiU FROM countries'
		const result = await pool.query(sql)
		if (result.length > 0) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'No Data Found' }).end()
		}
	} catch (err){
		console.log({
			message: err.text,
			errno: err.errno,
			code: err.code
		})
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const getRegions = async (req, res, next) => {
	try {
		const sql = `SELECT id, name FROM regions;`
		const result = await pool.query(sql)
		if (result.length > 0) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'No Data Found' }).end()
		}
	} catch (err){
		console.log({
			message: err.text,
			errno: err.errno,
			code: err.code
		})
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const getSubregions = async (req, res, next) => {
	let sql = ''
	try {
		if(isNaN(req.params.region)){
			console.log(isNaN(req.params.region))
			sql = `SELECT sr.id, sr.name FROM subregions sr 
					LEFT JOIN regions r ON (r.id = sr.region_id) 
					WHERE r.name = '${req.params.region}';`
		} else {
			console.log(isNaN(req.params.region))
			sql = `SELECT id, name FROM subregions 
					WHERE region_id = ${req.params.region};`
		}
		console.log(sql)
		const result = await pool.query(sql)
		if (result.length > 0) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'No Data Found' }).end()
		}
	} catch (err){
		console.log({
			message: err.text,
			errno: err.errno,
			code: err.code
		})
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const getCountriesRegion = async (req, res, next) => {
	console.log(req.params.region)
	try {
		const sql = `SELECT id, name, iso2, iso3, region, subregion, latitude, longitude, emoji, emojiU FROM countries c WHERE region = '${req.params.region}';`
		const result = await pool.query(sql)
		if (result.length > 0) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'No Data Found' }).end()
		}
	} catch (err){
		console.log({
			message: err.text,
			errno: err.errno,
			code: err.code
		})
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

export { getCountries, getRegions, getSubregions, getCountriesRegion }