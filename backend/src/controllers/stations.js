import pool from "../helpers/database.js"

const getStations = async (req, res, next) => {
	try {
		const sql = 'SELECT * FROM stations'
		const result = await pool.query(sql)
		if (result.length > 0) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'No Data Found' }).end()
		}
	} catch (err){
		console.log({
			message: err.text,
			errno: err.errno,
			code: err.code
		})
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const getStationId = async (req, res, next) => {
	try {
		const sql = `SELECT * FROM stations WHERE id = ${req.params.id}`
		const result = await pool.query(sql)
		if (result.length > 0) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'The station was not found.' }).end()
		}
	} catch (err){
		console.log({
			message: err.text,
			errno: err.errno,
			code: err.code
		})
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const postStations = async (req, res, next) => {
	try {
		const count = `SELECT count(*) as count FROM stations WHERE id = '${req.body.id}'`
		const rc = await pool.query(count)
		// Check if id already exists.
		if (rc[0].count > 0) {
			res.status(209).json({ message: 'This station id already exists.' });
		} else {
			const sql = `INSERT INTO stations (id, name, description, username, city_id, latitude, longitude) 
				VALUES ('${req.body.id}','${req.body.name}','${req.body.description}','${req.body.username}',${req.body.city},${req.body.latitude},${req.body.longitude})`
			const result = await pool.query(sql)
			res.status(201).json({ message: 'Station was successfully created.' });
		}
	} catch (err) {
		console.log({
			message: err.text,
			errno: err.errno,
			code: err.code
		})
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const putStations = async (req, res, next) => {
	try {
		const count = `SELECT count(*) as count FROM stations WHERE id = '${req.params.id}'`
		const rc = await pool.query(count)
		// Check if id already exists.
		if (rc[0].count > 0) {
			const sql = `UPDATE stations 
					SET name = '${req.body.name}', 
					description = '${req.body.description}', 
					owner_name = '${req.body.owner_name}', 
					owner_lastname = '${req.body.owner_lastname}', 
					owner_email = '${req.body.owner_email}', 
					owner_username = '${req.body.owner_username}', 
					city_id = ${req.body.city}, 
					latitude = ${req.body.latitude}, 
					longitude = ${req.body.longitude}
					WHERE id = '${req.params.id}';`
			const result = await pool.query(sql)
			res.status(204).json({ message: 'Station was succesfuly updated.' });
		} else {
			res.status(404).json({ message: 'The station was not found.' });
		}
	} catch (err) {
		console.log({
			message: err.text,
			errno: err.errno,
			code: err.code
		})
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const deleteStations = async (req, res, next) => {
	try {
		const count = `SELECT count(*) as count FROM stations WHERE id = '${req.params.id}'`
		const rc = await pool.query(count)
		// Check if id already exists.
		if (rc[0].count > 0) {
			const sql = `UPDATE stations SET active = false, deleted_at = CURRENT_TIMESTAMP WHERE id = '${req.params.id}';`
			const result = await pool.query(sql)
			console.log(result)
			res.status(204).json({ message: 'Station was succesfuly deactivated.' });
		} else {
			res.status(404).json({ message: 'The station was not found.' });
		}
	} catch (err) {
		console.log({
			message: err.text,
			errno: err.errno,
			code: err.code
		})
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

export { getStations, getStationId, postStations, putStations, deleteStations }