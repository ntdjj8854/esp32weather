import pool from "../helpers/database.js"

const getUnits = async (req, res, next) => {
	try {
		const sql = 'SELECT u.id, ud.id as dimension_id, ud.dimension, u.unit, u.representation FROM units u LEFT JOIN unit_dimension ud ON ud.id = u.dimension_id;'
		const result = await pool.query(sql)
		if (result.length > 0) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'No Data Found' }).end()
		}
	} catch (err){
		console.log({
			message: err.text,
			errno: err.errno,
			code: err.code
		})
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const getDimensions = async (req, res, next) => {
	try {
		const sql = 'SELECT * FROM unit_dimension ORDER BY dimension ASC;'
		const result = await pool.query(sql)
		if (result.length > 0) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'No Data Found' }).end()
		}
	} catch (err){
		console.log({
			message: err.text,
			errno: err.errno,
			code: err.code
		})
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

export { getUnits, getDimensions }