import pool from "../helpers/database.js"

const getSensors = async (req, res, next) => {
	try {
		const sql = 'SELECT * FROM sensors'
		const result = await pool.query(sql)
		if (result.length > 0) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'No Data Found' }).end()
		}
	} catch (err){
		// console.error(err)
		console.log({
			message: err.text,
			errno: err.errno,
			code: err.code
		})
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

const getStationSensors = async (req, res, next) => {
	try {
		const sql = `SELECT * FROM sensors WHERE station_id = ${req.params.id}`
		const result = await pool.query(sql)
		if (result.length > 0) {
			res.status(200).json(result)
		} else {
			res.status(404).send({ message : 'No Data Found' }).end()
		}
	} catch (err){
		console.log({
			message: err.text,
			errno: err.errno,
			code: err.code
		})
		res.status(500).json({ message: 'Something goes wrong' })
	}
}

export { getSensors, getStationSensors }