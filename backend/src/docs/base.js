import { config } from 'dotenv'
import tagsjs from './tags.js'

config()

const EMAIL = process.env.API_SUPPORT_EMAIL || "support@example.com"
const PORT = process.env.PORT || 3000

export default {
	openapi: "3.0.0",
	info: { 
		title: "ESP32Weather API",
		description: "This is the ESP32Weather API documentation.",
		termsOfService: "http://example.com/terms/",
		contact: {
			name: "API Support",
			url: "http://www.example.com/support",
			email: EMAIL
		},
		license: {
			name: "GNU GPLv3",
			url: "https://www.gnu.org/licenses/gpl-3.0.html"
		},
		version: "1.0.0"
	},
}

apis: ['./src/routes/*.js']