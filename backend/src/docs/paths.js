import moment from 'moment'

export default {
	paths: {
		"/api/users":{
			post: {
				summary: "Create a new user.",
				tags: ["Users"],
				requestBody: {
					required: true,
					content: {
						"application/json": {
							schema:{
								$ref: "#/components/schemas/User"
							}
						}
					}
				},
				responses:{
					201: {
						description: "User was successfully created"
					},
					209:{
						description: "This username id already exists."
					},
					500: {
						description: "Something goes wrong."
					}
				}
			}
		},
		"/api/users/{username}":{
			get: {
				summary: "Get the user data by username.",
				tags: ["Users"],
				parameters: [
          {
            name: "username",
            description: "The username to be fetch",
            required: true,
            in: "path",
            schema: {
              type: "string",
							example: "username"
            }
          }
        ],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/User"
								}
							}
						}
					},
					404: {
						description: "User not found for the given username.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			}
		},
		"/api/stations":{
			get: {
				summary: "Lists all the stations.",
				tags: ["Stations"],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/Station"
								}
							}
						}
					},
					404: {
						description: "Stations not found.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			},
			post: {
				summary: "Create a new station.",
				tags: ["Stations"],
				requestBody: {
					required: true,
					content: {
						"application/json": {
							schema:{
								$ref: "#/components/schemas/Station"
							}
						}
					}
				},
				responses:{
					201: {
						description: "Station was successfully created"
					},
					209:{
						description: "This station id already exists."
					},
					500: {
						description: "Something goes wrong."
					}
				}
			}
		},
		"/api/stations/{id}":{
			get: {
				summary: "Get the station by id.",
				tags: ["Stations"],
				parameters: [
          {
            name: "id",
            description: "The station id to be fetch",
            required: true,
            in: "path",
            schema: {
              type: "string",
							example: "ESP0001"
            }
          }
        ],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/Station"
								}
							}
						}
					},
					404: {
						description: "Station not found for the given id.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			},
			put: {
				summary: "Update a station by id.",
				tags: ["Stations"],
				requestBody: {
					required: true,
					content: {
						"application/json": {
							schema:{
								$ref: "#/components/schemas/Station"
							}
						}
					}
				},
				parameters: [
          {
            name: "id",
            description: "The station id to be updated",
            required: true,
            in: "path",
            schema: {
              type: "string",
							example: "ESP0001"
            }
          }
        ],
				responses:{
					204: {
						description: "Station was successfully updated.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			},
			delete: {
				summary: "Delete a station by id.",
				tags: ["Stations"],
				parameters: [
          {
            name: "id",
            description: "The station id to be deleted",
            required: true,
            in: "path",
            schema: {
              type: "string",
							example: "ESP0001"
            }
          }
        ],
				responses:{
					204: {
						description: "Station was successfully deactivated.",
					},
					404: {
						description: "Station was not found.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			}
		},
		"/api/sensors":{
			get: {
				summary: "Lists all the sensors.",
				tags: ["Sensors"],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/Sensor"
								},
								example:[
									{
										"id": 1,
										"station_id": "ESP0001",
										name: "Sensor1",
										description: "Sensor to capture temperature data",
										"manufacturer": "Temp Jan",
										"sensor_type": "Temperature sensor"
									},
									{
										"id": 2,
										"station_id": "ESP0002",
										name: "Sensor2",
										description: "Sensor to capture humidity data",
										"manufacturer": "Humi Xan",
										"sensor_type": "Himidity sensor"
									}
								]
							}
						}
					},
					404: {
						description: "No data found.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			}
		},
		"/api/sensors/{station_id}": {
			get: {
				summary: "Lists all the sensors of a station by his id.",
				tags: ["Sensors"],
				parameters: [
          {
            name: "station_id",
            description: "The station_id to be fetch",
            required: true,
            in: "path",
            schema: {
              type: "string",
							example: "ESP0001"
            }
          }
        ],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/Sensor"
								},
								example:[
									{
										"id": 1,
										"station_id": "ESP0001",
										name: "Sensor1",
										description: "Sensor to capture temperature data",
										"manufacturer": "Temp Jan",
										"sensor_type": "Temperature sensor"
									},
									{
										"id": 2,
										"station_id": "ESP0001",
										name: "Sensor2",
										description: "Sensor to capture humidity data",
										"manufacturer": "Humi Xan",
										"sensor_type": "Himidity sensor"
									}
								]
							}
						}
					},
					404: {
						description: "No data found.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			}
		},
		"/api/units": {
			get: {
				summary: "Lists all the units.",
				tags: ["Units"],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/Unit"
								},
								example: {
									"id": 1,
									"dimension_id": 1,
									"dimension": "temperature",
									"unit": "Celsius",
									"representation": "°C"
								}
							}
						}
					},
					404: {
						description: "No data found.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			}
		},
		"/api/dimensions": {
			get: {
				summary: "Lists all the regions.",
				tags: ["Dimension"],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/Dimension"
								},
								example: {
									id: 1,
									dimension: "temperature"
								}
							}
						}
					},
					404: {
						description: "No data found.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			}
		},
		"/api/regions": {
			get: {
				summary: "Lists all the regions.",
				tags: ["World"],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/Region"
								},
								example: {
									"id": 4,
									name: "Europe"
								}
							}
						}
					},
					500: {
						description: "Something goes wrong."
					}
				}
			}
		},
		"/api/subregions/{region_id}": {
			get: {
				summary: "Lists all the subregions of a region by his id.",
				tags: ["World"],
				parameters: [
          {
            name: "region_id",
            description: "Id of Region",
            required: true,
            in: "path",
            schema: {
              type: "integer",
							example: 4
            }
          }
        ],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/Subregion"
								},
								example: {
									id: 15,
									name: "Northern Europe"
								}
							}
						}
					},
					404: {
						description: "Subregions not found for the given region_id.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			}
		},
		"/api/subregions/{region}": {
			get: {
				summary: "Lists all the subregions of a region by his name.",
				tags: ["World"],
				parameters: [
          {
            name: "region",
            description: "Name of Region",
            required: true,
            in: "path",
            schema: {
              type: "string",
							example: "Europe"
            }
          }
        ],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/Subregion"
								},
								example: {
									"id": 15,
									name: "Northern Europe"
								}
							}
						}
					},
					404: {
						description: "Subregions not found for the given region_name.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			}
		},
		"/api/countries": {
			get: {
				summary: "Lists all the countries.",
				tags: ["World"],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/Country"
								},
								// example: {
								// 	"id": 4,
								// 	name: "Europe"
								// }
							}
						}
					},
					404: {
						description: "No data found.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			}
		},
		"/api/countries/{region}": {
			get: {
				summary: "Lists all the countries.",
				tags: ["World"],
				parameters: [
          {
            name: "region",
            description: "Name of Region",
            required: true,
            in: "path",
            schema: {
              type: "string",
							example: "Europe"
            }
          }
        ],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/Country"
								},
								example: {
									"id": 4,
									name: "Europe"
								}
							}
						}
					},
					404: {
						description: "No data found.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			}
		},
		"/api/readings/{station_id}":{
			get: {
				summary: "Get last 20 readings of a station by station_id.",
				tags: ["Readings"],
				parameters: [
          {
            name: "station_id",
            description: "The station id to be fetch",
            required: true,
            in: "path",
            schema: {
              type: "string",
							example: "ESP0001"
            }
          }
        ],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/StationReadings"
								}
							}
						}
					},
					404: {
						description: "Readings not found for the given station_id.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			},
			post:{
				summary: "Insert new readings.",
				tags: ["Readings"],
				parameters: [
					{
            name: "station_id",
            description: "The station_id to set new readings",
            required: true,
            in: "path",
            schema: {
              type: "string",
							example: "ESP0001"
            }
          }
				],
				requestBody: {
					required: true,
					content: {
						"application/json": {
							schema:{
								type: "object",
								properties: {
									"temperature": {
										type: "number",
										format: "double",
										example: 23.5
									},
									"temp_unit_id": {
										type: "integer",
										format: "int64",
										example: 1
									},
									"humidity": {
										type: "number",
										format: "double",
										example: 58.5
									},
									"hum_unit_id": {
										type: "integer",
										format: "int64",
										example: 4
									},
									"pressure": {
										type: "number",
										format: "double",
										example: 1023.4
									},
									"pres_unit_id": {
										type: "integer",
										format: "int64",
										example: 5
									}
								}
							}
						}
					}
				},
				responses: {
					201: {
						description: "Data was successfully inserted."
					},
					500: {
						description: "Something goes wrong."
					}
				}
			}
		},
		"/api/readings/{station_id}/{start_date}/{end_date}":{
			get: {
				summary: "Get readings between two dates of a station.",
				tags: ["Readings"],
				parameters: [
          {
            name: "station_id",
            description: "The station id to be fetch",
            required: true,
            in: "path",
            schema: {
              type: "string",
							example: "ESP0001"
            }
          },
					{
            name: "start_date",
            description: "Start date",
            required: true,
            in: "path",
            schema: {
              type: "string",
							format: "date",
							example: moment(new Date()).format("YYYY-MM-DD")
            }
          },
					{
            name: "end_date",
            description: "End date",
            required: true,
            in: "path",
            schema: {
              type: "string",
							format: "date",
							example: moment(new Date()).format("YYYY-MM-DD")
            }
          }
        ],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/Readings"
								}
							}
						}
					},
					404: {
						description: "No data found",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			}
		},
		"/api/readings/{station_id}/{dimension}":{
			get: {
				summary: "Get last 20 readings of a dimension of a station by station_id.",
				tags: ["Readings"],
				parameters: [
          {
            name: "station_id",
            description: "The station id to be fetch",
            required: true,
            in: "path",
            schema: {
              type: "string",
							example: "ESP0001"
            }
          },
					{
            name: "dimension",
            description: "Dimension of readings",
            required: true,
            in: "path",
            schema: {
              type: "string",
							example: 'temperature'
            }
          }
        ],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/DimensionReadings"
								}
							}
						}
					},
					404: {
						description: "No data found.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			},
		},
		"/api/readings/{station_id}/{dimension}/{start_date}/{end_date}":{
			get: {
				summary: "Get dimension readings between two dates of a station by station_id.",
				tags: ["Readings"],
				parameters: [
          {
            name: "station_id",
            description: "The station id to be fetch",
            required: true,
            in: "path",
            schema: {
              type: "string",
							example: "ESP0001"
            }
          },
					{
            name: "dimension",
            description: "Dimension of readings",
            required: true,
            in: "path",
            schema: {
              type: "string",
							example: 'temperature'
            }
          },
					{
            name: "start_date",
            description: "Start date",
            required: true,
            in: "path",
            schema: {
              type: "string",
							format: "date",
							example: moment(new Date()).format("YYYY-MM-DD")
            }
          },
					{
            name: "end_date",
            description: "End date",
            required: true,
            in: "path",
            schema: {
              type: "string",
							format: "date",
							example: moment(new Date()).format("YYYY-MM-DD")
            }
          }
        ],
				responses:{
					200: {
						description: "OK",
						content: {
							"application/json": {
								schema:{
									$ref: "#/components/schemas/DimensionReadings"
								}
							}
						}
					},
					404: {
						description: "No data found.",
					},
					500: {
						description: "Something goes wrong."
					}
				}
			},
		},
	}
}