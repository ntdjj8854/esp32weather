import base from "./base.js";
import servers from "./servers.js";
import tags from "./tags.js";
import paths from "./paths.js";
import components from './components.js'

export default {
	...base,
	...servers,
	...tags,
	...paths,
	...components
}