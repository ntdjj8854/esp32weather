import { check } from 'express-validator'
import { validateResult } from '../helpers/validateHelper.js'

const validateStation = [
	check('id')
		.exists()
		.isAlphanumeric()
		.isLength({min:5})
		.not()
		.isEmpty(),
	check('name')
		.exists()
		.not()
		.isEmpty(),
	check('description')
		.exists()
		.not()
		.isEmpty(),
	check('username')
		.exists()
		.not()
		.isEmpty(),
	check('city')
		.exists()
		.isInt()
		.not()
		.isEmpty(),
	check('latitude')
		.exists()
		.not()
		.isEmpty(),
	check('longitude')
		.exists()
		.not()
		.isEmpty(),
	(req, res, next) => {
		validateResult(req, res, next)
	}
]

export default validateStation;