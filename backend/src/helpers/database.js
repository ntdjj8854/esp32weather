import mariadb from 'mariadb'
import { DBHOST, DBPORT, DBNAME, DBUSER, DBPASS, DBDIALECT} from '../config/database.config.js';

const pool = mariadb.createPool({
  host: DBHOST,
  user: DBUSER,
	port: DBPORT,
  database: DBNAME,
  password: DBPASS,
  connectionLimit: 5
});

export default pool