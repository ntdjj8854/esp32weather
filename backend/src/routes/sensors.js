import { Router } from 'express'
import * as sensorsController from '../controllers/sensors.js'

const router = Router()

router.get('/sensors', sensorsController.getSensors)
router.get('/sensors/:station_id', sensorsController.getStationSensors)

export default router