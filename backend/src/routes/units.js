import { Router } from 'express'
import * as unitsController from '../controllers/units.js'

const router = Router()		// Ejecutamos el router.

router.get('/units', unitsController.getUnits)
router.get('/dimensions', unitsController.getDimensions)

export default router