import { Router } from 'express'
import * as readingsController from '../controllers/readings.js'
import validateReadings from '../validators/readings.js'

const router = Router()

router.get('/readings/latest/:station_id', readingsController.getStationLastReadings) //Return last 24h readings
router.get('/readings/:station_id', readingsController.getStationReadings) //Return last 24h readings
router.get('/readings/:station_id/:start_date/:end_date', readingsController.getStationReadingsByDates)
router.get('/readings/:station_id/:dimension', readingsController.getStationReadingsByDimension)
router.get('/readings/:station_id/:dimension/:start_date/:end_date', readingsController.getStationReadingsByDimensionByDates)
router.post('/readings/:station_id', validateReadings, readingsController.postStationReadings)

export default router