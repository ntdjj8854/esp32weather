# Implemented Endpoints

## Stations

GET /api/stations
POST /api/stations
GET /api/stations/{id} -- TODO
DELETE /api/stations/{id}  -- TODO

## Sensors

GET /sensors
GET /sensors/{id} -- TODO
DELETE /sensors/{id} -- TODO
GET /sensors/{station_id}
POST /sensors/{station_id} -- TODO

## Units

GET /api/units
GET /api/dimensions

Getting info of countries from [Country State City API](https://countrystatecity.in).

[Github Country State City API](https://github.com/dr5hn/countries-states-cities-database)
