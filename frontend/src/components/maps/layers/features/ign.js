import TileLayer from 'ol/layer/Tile';
import TileWMS from 'ol/source/TileWMS';

export async function getIGNLayers(layerGroup){
	let response = await fetch('/ign.json');
	let services = await response.json();
	for (let service of services) {
		for (let layer of service.layers){
			layerGroup.getLayers().push(new TileLayer({
				title: layer.title,
				visible: false,
				source: new TileWMS({
					url: service.url,
					crossOrigin: 'anonymous',
					params: { 
						"LAYERS": layer.name 
					}
				})
			}))
		}
	}
}