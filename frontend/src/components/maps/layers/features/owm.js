import OSM from 'ol/source/OSM';
import TileLayer from 'ol/layer/Tile';

const API_OWM_KEY = import.meta.env.VITE_API_OWM_KEY

export const templayer = new TileLayer({
	title: 'Temperature Layer',
	visible: false,
  source: new OSM({
    attributions: [
      ' © <a href="https://openweathermap.org">OpenWeatherMap</a>',
    ],
    opaque: false,
    url: `http://tile.openweathermap.org/map/temp_new/{z}/{x}/{y}.png?appid=${API_OWM_KEY}`,
  }),
});

export const cloudslayer = new TileLayer({
	title: 'OClouds Layer',
	visible: false,
  source: new OSM({
    attributions: [
      ' © <a href="https://openweathermap.org">OpenWeatherMap</a>',
    ],
    opaque: false,
    url: `http://tile.openweathermap.org/map/clouds_new/{z}/{x}/{y}.png?appid=${API_OWM_KEY}`,
  }),
});

export const precipitationlayer = new TileLayer({
	title: 'Precipitation Layer',
	visible: false,
  source: new OSM({
    attributions: [
      ' © <a href="https://openweathermap.org">OpenWeatherMap</a>',
    ],
    opaque: false,
    url: `http://tile.openweathermap.org/map/precipitation_new/{z}/{x}/{y}.png?appid=${API_OWM_KEY}`,
  }),
});

export const pressurelayer = new TileLayer({
	title: 'Pressure Layer',
	visible: false,
  source: new OSM({
    attributions: [
      ' © <a href="https://openweathermap.org">OpenWeatherMap</a>',
    ],
    opaque: false,
    url: `http://tile.openweathermap.org/map/pressure_new/{z}/{x}/{y}.png?appid=${API_OWM_KEY}`,
  }),
});

export const windlayer = new TileLayer({
	title: 'Wind Layer',
	visible: false,
  source: new OSM({
    attributions: [
      ' © <a href="https://openweathermap.org">OpenWeatherMap</a>',
    ],
    opaque: false,
    url: `http://tile.openweathermap.org/map/wind_new/{z}/{x}/{y}.png?appid=${API_OWM_KEY}`,
  }),
});