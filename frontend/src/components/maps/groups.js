import LayerGroup from 'ol/layer/Group'

import { osm, watercolor, terrain, toner, opentopomap, memomaps_transport } from './layers/base/carto_base'
import { topoLayer, streets, maptiler_outdoor } from './layers/base/maptiler_base'
import { opencyclemap, transport, landscape, outdoors, transport_dark, pioneer, mobile_atlas, neighbourhood } from './layers/base/thunderforest_base'
import { mapbox_streets, mapbox_outdoors,	mapbox_satellite,	mapbox_satellite_streets,	mapbox_navigation_day, mapbox_navigation_night } from './layers/base/mapbox_base'
import { openseamap } from './layers/features/mixed'
import { templayer, cloudslayer, precipitationlayer, pressurelayer, windlayer } from './layers/features/owm'

export const carto_base = new LayerGroup({
	'title': 'Carto Base',
	combine: false,
	fold: 'close',
	layers: [
		osm,
		watercolor,
		terrain,
		toner,
		opentopomap,
		memomaps_transport,
	]
})

export const maptiler_base = new LayerGroup({
	'title': 'Maptiler Base',
	combine: false,
	fold: 'close',
	layers: [
		topoLayer,
		streets,
		maptiler_outdoor
	]
})

export const mapbox_base = new LayerGroup({
	'title': 'Mapbox Layers',
	combine: false,
	fold: 'close',
	layers: [
		mapbox_streets,
		mapbox_outdoors,
		mapbox_satellite,
		mapbox_satellite_streets,
		mapbox_navigation_day,
		mapbox_navigation_night
	]
})

export const thunderforest_base = new LayerGroup({
	'title': 'Thunderforest Layers',
	combine: false,
	fold: 'close',
	layers: [
		new LayerGroup({
			'title': 'Thunderforest Group1',
			combine: false,
			fold: 'close',
			layers: [
				opencyclemap,
				transport,
				landscape,
				outdoors
			]
		}),
		new LayerGroup({
			'title': 'Thunderforest Group1',
			combine: false,
			fold: 'close',
			layers: [
				transport_dark,
				pioneer,
				mobile_atlas,
				neighbourhood
			]
		}),
	]
})

export const layers = new LayerGroup({
	'title': 'Other Layers',
	combine: false,
	fold: 'close',
	layers: [
		openseamap
	]
})

export const ign = new LayerGroup({
	'title': 'IGN Layers',
	combine: false,
	fold: 'close',
	layers: [	]
})

export const owm_layers = new LayerGroup({
	'title': 'OWM Layers',
	combine: false,
	fold: 'close',
	layers: [templayer, cloudslayer, precipitationlayer, pressurelayer, windlayer]
})