import { createApp } from 'vue'
import App from './App.vue'
// import './style.css'
import router from './router/index'
import axios from 'axios'

import { createAutoAnimatePlugin } from '@formkit/addons'
import { plugin, defaultConfig } from '@formkit/vue'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'

import '@formkit/themes/genesis'
import 'mosha-vue-toastify/dist/style.css'

library.add(fas)

createApp(App)
.use(plugin, defaultConfig ({
	plugins: [
    createAutoAnimatePlugin({
      // optional config
    })
  ]
}))
.use(router, axios)
.component('fa', FontAwesomeIcon)
.mount('#app')