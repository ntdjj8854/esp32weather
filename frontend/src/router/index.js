import { createRouter, createWebHistory } from "vue-router";

import Dashboard from "@/views/Dashboard.vue"
import UserInfo from '@/views/UserInfo.vue'
import MapView from "@/views/MapView.vue";
import Contact from "@/views/Contact.vue";
import About from "@/views/About.vue";
import DimensionView from "@/views/DimensionView.vue";

const STATION_ID = import.meta.env.VITE_STATION_ID

const routes = [
  { 
		path: "/", 
		name: "Home",
		component: Dashboard 
	},
  { 
		path: "/user", 
		name: "User",
		component: UserInfo 
	},
	{ 
		path: "/temperature", 
		name: "Temperature",
		component: DimensionView,
		props: {
			unit: "°C",
			bgcolor: '#F44545',
			bcolor: '#f87979',
			pbcolor: '#F44545',
			station_id: STATION_ID,
			dimension: 'temperature',
		}
	},
	{ 
		path: "/humidity", 
		name: "Humidity",
		component: DimensionView,
		props: {
			unit: "%",
			bgcolor: '#4552f4',
			bcolor: '#5560ff',
			pbcolor: '#4552f4',
			station_id: STATION_ID,
			dimension: 'humidity',
		}
	},
	{ 
		path: "/pressure", 
		name: "Pressure",
		component: DimensionView,
		props: {
			unit: "%",
			bgcolor: '#d67100',
			bcolor: '#ee9d40',
			pbcolor: '#d67100',
			station_id: STATION_ID,
			dimension: 'pressure',
		} 
	},
	{ 
		path: "/about", 
		name: "About",
		component: About 
	},
  { 
		path: "/dashboard",
		name: "Dashboard",
		component: Dashboard 
	},
	{ 
		path: "/contact",
		name: "Contact",
		component: Contact 
	},
	{ 
		path: "/map",
		name: "MapView",
		component: MapView 
	},
];

const history = createWebHistory();

const router = createRouter({
	history,
	routes,
});

export default router